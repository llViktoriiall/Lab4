﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Collections;
using Newtonsoft.Json;

namespace Game_0._1
{
    public partial class Form1 : Form
    {
        int Und = 16;
        int Gold = 0;
        int Iron = 0;
        int Coal = 0;
        public Form1()
        {
            InitializeComponent();
            pictureBox1.Image = Properties.Resources.map;
            pictureBox2.Image = Properties.Resources.rec_map;
            pictureBox3.Image = Properties.Resources.rec_map;
            pictureBox4.Image = Properties.Resources.rec_map;
            pictureBox5.Image = Properties.Resources.gold;
            pictureBox6.Image = Properties.Resources.rec_map;
            pictureBox7.Image = Properties.Resources.rec_map;
            pictureBox8.Image = Properties.Resources.rec_map;
            pictureBox9.Image = Properties.Resources.rec_map;
            pictureBox10.Image = Properties.Resources.rec_map;
            pictureBox11.Image = Properties.Resources.rec_map;
            pictureBox12.Image = Properties.Resources.rec_map;
            pictureBox13.Image = Properties.Resources.rec_map;
            pictureBox14.Image = Properties.Resources.coal;
            pictureBox15.Image = Properties.Resources.rec_map;
            pictureBox16.Image = Properties.Resources.iron;
            pictureBox17.Image = Properties.Resources.rec_map;
        }

        private void button1_Click(object sender, EventArgs e)
        {
        }

        info current = new info();

        private void pictureBox16_Click(object sender, EventArgs e)
        {
            Und--;
            Iron++;
            current.unDiscovered = Und;
            current.iron = Iron;
            richTextBox1.Text = richTextBox1.Text + "\nYou have found iron!\n";
            string saved;
            saved = "\nCurrent state:\n";
            saved += "Undiscovered parts of the map - " + Und + "\n";
            saved += "Gold - " + Gold + "\n";
            saved += "Iron - " + Iron + "\n";
            saved += "Coal - " + Coal + "\n";
            richTextBox1.Text = richTextBox1.Text + saved;
        }

        private void pictureBox14_Click(object sender, EventArgs e)
        {
            Und--;
            Coal++;
            current.unDiscovered = Und;
            current.coal = Coal;
            richTextBox1.Text = richTextBox1.Text + "\nYou have found coal!\n";
            string saved;
            saved = "\nCurrent state:\n";
            saved += "Undiscovered parts of the map - " + Und + "\n";
            saved += "Gold - " + Gold + "\n";
            saved += "Iron - " + Iron + "\n";
            saved += "Coal - " + Coal + "\n";
            richTextBox1.Text = richTextBox1.Text + saved;
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            Und--;
            Gold++;
            current.unDiscovered = Und;
            current.gold = Gold;
            richTextBox1.Text = richTextBox1.Text + "\nYou have found gold!\n";
            string saved;
            saved = "\nCurrent state:\n";
            saved += "Undiscovered parts of the map - " + Und + "\n";
            saved += "Gold - " + Gold + "\n";
            saved += "Iron - " + Iron + "\n";
            saved += "Coal - " + Coal + "\n";
            richTextBox1.Text = richTextBox1.Text + saved;
        }
        Context c = new Context(new ConcreteStateA());
        ConcreteStateA st1 = new ConcreteStateA();
        ConcreteStateB st2 = new ConcreteStateB();
        ConcreteStateC st3 = new ConcreteStateC();
        int counter = 0;
        private void pictureBox12_Click(object sender, EventArgs e)
        {
            string info;
            counter++;
            switch (counter)
            {
                case 1:
                    c.TransitionTo(st1);
                    pictureBox12.Image = Properties.Resources.b1;
                    info = c.Request1();
                    richTextBox1.Text = richTextBox1.Text + info;

                    break;
                case 2:
                    c.TransitionTo(st2);
                    pictureBox12.Image = Properties.Resources.b2;
                    info = c.Request2();
                    richTextBox1.Text = richTextBox1.Text + info;
                    break;
                case 3:
                    c.TransitionTo(st3);
                    pictureBox12.Image = Properties.Resources.b3;
                    info = c.Request3();
                    richTextBox1.Text = richTextBox1.Text + info;
                    break;
            }
        }

        Originator o = new Originator();
        private void button3_Click(object sender, EventArgs e)
        {
            o.CreateMemento(current);
            string temp = o.getSaved();
            richTextBox1.Text = richTextBox1.Text + temp;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            current = JsonConvert.DeserializeObject<info>(o.getSaved());
            richTextBox1.Text = richTextBox1.Text + "Everything is back!";
            Und = current.unDiscovered;
            Gold = current.gold;
            Iron = current.iron;
            Coal = current.coal;
            richTextBox1.Text = richTextBox1.Text + "\nUndiscovered" + current.unDiscovered + "\nGold" + current.gold + "\nIron" + current.iron + "\nCoal" + current.coal;
        }
    }

    //поведінкові шаблони1

    //будівля з трьома станами: будується, збудована, модернізована

    class Context
    {
        private State _state = null;

        public Context(State state)
        {
            this.TransitionTo(state);
        }
        public void TransitionTo(State state)
        {
            this._state = state;
            this._state.SetContext(this);
        }
        public string Request1()
        {
            this._state.ShowState();

            return "\nYour factory is in building process.\nIt is producing with speed " + _state.time;
        }

        public string Request2()
        {
            this._state.ShowState();
            return "\nYour factory has been builded.\nIt is producing with speed " + _state.time;
        }

        public string Request3()
        {
            this._state.ShowState();
            return "\nYour factory is updated now.\nIt is producing with speed " + _state.time;
        }

    }

    abstract class State
    {
        protected Context _context;
        public int time;

        public void SetContext(Context context)
        {
            this._context = context;
        }

        public abstract void ShowState();

    }

    class ConcreteStateA : State
    {
        public override void ShowState()
        {
            time = 0;
        }

    }
    class ConcreteStateB : State
    {
        public override void ShowState()
        {
            time = 4;
        }
    }
    class ConcreteStateC : State
    {
        public override void ShowState()
        {
            time = 8;
        }
    }







    //сейвим інфу про кіькість ресурсів і ще недосліджених частин карти

    //!!!!!!ЗРОБИТИ СЕРІАЛІЗАЦІЮ!!!!!!!!!

    class info
    {
        public int unDiscovered = 16;
        public int gold = 0;
        public int iron = 0;
        public int coal = 0;
    }

    class Originator
    {
        private string _state;
        public string getSaved()
        {
            return _state;
        }

        public Memento CreateMemento(info current)
        {
            _state = JsonConvert.SerializeObject(current);
            return (new Memento(_state));
        }



        public void SetMemento(Memento memento)
        {
            State = memento.State;
        }

        public string State
        {
            get { return _state; }
            set
            {
                _state = value;
            }
        }
    }

    class Memento
    {
        private string _state;

        public Memento(string state)
        {
            this._state = state;
        }
        public string State
        {
            get { return _state; }
        }
    }



}
